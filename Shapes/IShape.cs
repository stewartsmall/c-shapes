﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public interface IShape
    {
        int Area();
        int Circumfrence();
        void Run();
        void GetInput();
    }
}
