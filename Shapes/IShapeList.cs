﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public interface IShapeList
    {
        void Add(IShape shape);
        void Remove(IShape shape);
        IEnumerable<IShape> GetShapes();
    }
}
