﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public class Triangle : IShape
    {
        private int Height;
        private int Base;
        private int Side;

        public int Area()
        {
            return (Base * Height) / 2;
        }

        public int Circumfrence()
        {
            return Base + Height + Side;
        }

        public void GetInput()
        {
            Console.WriteLine("Enter the height of the Trinagle:");
            Height = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter the base of the Trinagle:");
            Base = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter the diagonal height of the Trinagle:");
            Side = Convert.ToInt32(Console.ReadLine());
        }

        public void Run()
        {
            Console.WriteLine("Area: " + Area());
            Console.WriteLine("Perimeter: " + Circumfrence());
        }
    }
}
