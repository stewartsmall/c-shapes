﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public class CalculateEngine
    {
        public void Execute(IShapeList shapes)
        {
            foreach(var shape in shapes.GetShapes())
            {
                shape.GetInput();
                shape.Run();
            }
        }
    }
}
