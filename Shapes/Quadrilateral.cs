﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public abstract class Quadrilateral : IShape
    {
        protected int Length;
        protected int Width;
        protected int Height;

        protected string input;

        public Quadrilateral(int length, int width, int height)
        {
            Length = length;
            Width = width;
            Height = height;
        }

        public Quadrilateral(int length, int width)
        {
            Length = length;
            Width = width;
        }

        public Quadrilateral(int length)
        {
            Length = length;
        }

        public Quadrilateral()
        {
        }

        public virtual int Area()
        {
            return Length * Width;
        }

        public virtual int Circumfrence()
        {
            return (Length + Width) * 2;
        }

        public abstract void GetInput();

        public void Run()
        {
            Console.WriteLine("Area: " + Area());
            Console.WriteLine("Perimeter: " + Circumfrence());
        }
    }
}
