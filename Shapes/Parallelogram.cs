﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public class Parallelogram : Quadrilateral
    {
        public Parallelogram(int length, int width, int height) : base(length, width, height)
        {
        }

        public Parallelogram()
        {
        }

        public override int Area()
        {
            return Height * Width;
        }

        public override int Circumfrence()
        {
            return (Height + Length) * 2;
        }

        public override void GetInput()
        {
            Console.WriteLine("Enter the length of the Parallelogram: ");
            Length = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter the width of the Parallelogram: ");
            Width = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter the height of the Parallelogram: ");
            Height = Convert.ToInt32(Console.ReadLine());
        }
    }
}
