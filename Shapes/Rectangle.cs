﻿using NCalc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public class Rectangle : Quadrilateral
    {
        public Rectangle(int length, int width) : base(length, width)
        {
        }

        public Rectangle()
        {
        }

        public override void GetInput()
        {
            Console.WriteLine("Enter the length of the Rectangle: ");
            Length = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter the width of the Rectangle: ");
            Width = Convert.ToInt32(Console.ReadLine());
        }
    }
}
