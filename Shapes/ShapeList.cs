﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public class ShapeList : IShapeList
    {
        private List<IShape> Shapes;

        public ShapeList()
        {
            Shapes = new List<IShape>();
        }

        public virtual void Add(IShape shape)
        {
            Shapes.Add(shape);
        }

        public virtual void Remove(IShape shape)
        {
            Shapes.Remove(shape);
        }

        public virtual IEnumerable<IShape> GetShapes()
        {
            return Shapes;
        }
    }
}
