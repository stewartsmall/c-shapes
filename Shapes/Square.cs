﻿using NCalc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public class Square : Quadrilateral
    {
        public Square(int length) : base(length)
        {
        }

        public Square()
        {
        }

        public override int Area()
        {
            return Length * Length;
        }

        public override int Circumfrence()
        {
            return 2 * Length;
        }

        public override void GetInput()
        {
            Console.WriteLine("Enter the length of the Square: ");
            Length = Convert.ToInt32(Console.ReadLine());
        }
    }
}
