﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    class Program
    {
        static void Main(string[] args)
        {
            var shapes = new ShapeList();

            shapes.Add(new Square());
            shapes.Add(new Rectangle());
            shapes.Add(new Parallelogram());
            shapes.Add(new Triangle());

            var engine = new CalculateEngine();
            engine.Execute(shapes);

            Console.ReadLine();
        }
    }
}
